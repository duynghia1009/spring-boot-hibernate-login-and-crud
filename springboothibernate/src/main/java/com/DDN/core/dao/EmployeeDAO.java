package com.DDN.core.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.DDN.core.model.Employee;
import com.DDN.core.repository.EmployeeRepository;

@Service
public class EmployeeDAO {
	@Autowired
	EmployeeRepository studentRepository;
	
	/*to save an employee*/
	
	public Employee save(Employee std) {
		return studentRepository.save(std);
	}
	
	
	/* search all employees*/
	
	public List<Employee> findAll(){
		return studentRepository.findAll();
	}
	
	
	/*get an employee by id*/
	public Employee findOne(Integer id) {
		return studentRepository.findOne(id);
	}
	
	
	/*delete an employee*/
	
	public void delete(Employee std) {
		studentRepository.delete(std);
	}
	

}
