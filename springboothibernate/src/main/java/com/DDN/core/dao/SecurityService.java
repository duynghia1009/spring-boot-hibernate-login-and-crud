package com.DDN.core.dao;



public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
