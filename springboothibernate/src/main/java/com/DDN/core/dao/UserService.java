package com.DDN.core.dao;

import com.DDN.core.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
