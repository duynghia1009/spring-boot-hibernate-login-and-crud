package com.DDN.core.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.DDN.core.dao.SecurityService;
import com.DDN.core.dao.EmployeeDAO;
import com.DDN.core.dao.UserService;
import com.DDN.core.model.Employee;
import com.DDN.core.model.User;
import com.DDN.core.validator.UserValidator;



@Controller
public class EmployeeController {
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
    private UserService userService;
	
	@Autowired
    private SecurityService securityService;
	
	 @Autowired
	    private UserValidator userValidator;
	
	@RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/viewemployees";
    }
	
	 @RequestMapping(value = "/login", method = RequestMethod.GET)
	    public String login(Model model, String error, String logout) {
	        if (error != null)
	            model.addAttribute("error", "Your username and password is invalid.");

	        if (logout != null)
	            model.addAttribute("message", "You have been logged out successfully.");

	        return "login";
	    }
	
	
	@RequestMapping(value= "/enroll",method=RequestMethod.GET)
	public String newRegistration(ModelMap model) {
		Employee employee = new Employee();
		model.addAttribute("employee",employee);
		return "enroll";
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String saveRegistration(@Valid Employee employee,BindingResult result,ModelMap model,RedirectAttributes redirectAttributes) {
		
		if(result.hasErrors()) {
			return "enroll";
		}
		
		employeeDAO.save(employee);
		
		return "redirect:/viewemployees";
	}
	
	
	@RequestMapping(value= {"/","/viewemployees"})
	public ModelAndView getAll() {
		
		List<Employee> list=employeeDAO.findAll();
		return new ModelAndView("viewemployees","list",list);
	}
	
	
	@RequestMapping(value="/editemployee/{id}")
	public String edit (@PathVariable int id,ModelMap model) {
		
		Employee employee=employeeDAO.findOne(id);
		model.addAttribute("employee",employee);
		return "editemployee";
	}
	
	@RequestMapping(value="/editsave",method=RequestMethod.POST)
	public ModelAndView editsave(@ModelAttribute("employee") Employee p) {
		
		Employee employee=employeeDAO.findOne(p.getId());
		
		employee.setFirstName(p.getFirstName());
		employee.setLastName(p.getLastName());
		employee.setCountry(p.getCountry());
		employee.setEmail(p.getEmail());
		employee.setSection(p.getSection());
		employee.setSex(p.getSex());
		
		employeeDAO.save(employee);
		return new ModelAndView("redirect:/viewemployees");
	}
	
	@RequestMapping(value="/deleteemployee/{id}",method=RequestMethod.GET)
	public ModelAndView delete(@PathVariable int id) {
		Employee employee=employeeDAO.findOne(id);
		employeeDAO.delete(employee);
		return new ModelAndView("redirect:/viewemployees");
	}
	
	

	@ModelAttribute("sections")
	public List<String> intializeSections(){
		List<String> sections = new ArrayList<String>();
		sections.add("HR");
		sections.add("DEV Web");
		sections.add("DEV Embedded");
		return sections;
	}
	
	
	/*
	 * Method used to populate the country list in view. Note that here you can
	 * call external systems to provide real data.
	 */
	@ModelAttribute("countries")
	public List<String> initializeCountries() {

		List<String> countries = new ArrayList<String>();
		countries.add("Tien Giang");
		countries.add("Ben Tre");
		countries.add("Long An");
		countries.add("Vinh Long");
		countries.add("Can Tho");
		countries.add("Ha Noi");
		countries.add("OTHER");
		return countries;
	}

	
	
	

}
