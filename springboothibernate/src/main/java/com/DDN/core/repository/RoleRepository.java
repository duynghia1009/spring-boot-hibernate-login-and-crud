package com.DDN.core.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.DDN.core.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
