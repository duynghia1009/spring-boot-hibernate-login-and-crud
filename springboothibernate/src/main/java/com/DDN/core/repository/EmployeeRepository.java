package com.DDN.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.DDN.core.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
