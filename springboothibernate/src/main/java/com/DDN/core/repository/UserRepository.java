package com.DDN.core.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.DDN.core.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
